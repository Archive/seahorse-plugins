If you want to hack or commit something to seahorse, please first send patches to 
seahorse-list@gnome.org or submitted on bugzilla.gnome.org if possible.

For a todo list, first check bugzilla, then look at TODO. Bugzilla is also updated 
with possible enhancements and any current bugs.

Here is a description of the modules:

 o data: Data files that need to be processed. Current this is just schemas. They 
   are here because there will be multiple schemas files to separate the app and 
   pgp settings.

 o libseahorse: Common static library of code for seahorse. Any code that is depended 
   on by multiple modules, such as src/ and plugins/nautilus, is placed here. 
   Currently this includes SeahorseContext & keys, operations, and common dialogs.

 o plugins: Contains subdirs for any possible apps in which seahorse can provide 
   a plugin. Currently this includes nautilus and gedit. Future possibilities are 
   AbiWord and Gaim.

 o plugins/nautilus: Contains the seahorse-tool for nautilus encryption. Also has 
   the seahorse-prefercences capplet (which should be moved once we get into SVN).
   The MIME data is also located here.

 o plugins/nautilus-ext: An extension for Nautilus 2.10 and later.

 o plugins/gedit: The gedit plugin.
 
 o plugins/applet: Clipboard encryption panel applet


USING THE GPG EXECUTABLE DIRECTLY

Unless absolutely necessary seahorse limits itself to using the functionality 
found in the GPGME library. This is to prevent maintenance, versioning, syntax
and regression problems that arise from parsing the gpg executable output 
directly. 

In some cases the GPGME doesn't have a given feature that is necessary for 
Seahorse's operation. This may be due to the GPGME developers not having 
implemented that feature yet, or refusal to implement a given feature. In 
these cases direct use of GPG is permitted. Such cases must be documented 
below, and must be discussed before hand on the seahorse-devel mailing list.

 o Photo ID support
   - gpgme_op_edit state machine in libseahorse/seahorse-pgp-key-op.c 
   
 o Export Secret Key Support 
   - libseahorse/seahorse-gpgmex.c


DEBUGGING SEAHORSE-PLUGINS

There are a couple of configure switches that you can enable which add
debugging helpers etc...

--enable-debug 
    - Disables optimization
    - Debug info
    - Enables various checks and logging in the code.
    - Makes all Gtk|Glib etc... runtime warnings fatal.
    - Makes all compilation warnings fatal.
    
--enable-fatal-messages
    When used with --enable-debug turns on fatal compile and runtime warnings.

INDENTATION STYLE

  * No gratituous reformatting of code. If you're rewriting much of a function 
    or file it's okay to reformat it, but reformatting for the sake of it isn't 
    a good idea.

    static int 
    function (int x, const char *y) 
    {
        if (func (arg, arg2 + 1, lots, of, very, lengthy, arguments, for 
                  this, function)) {
                  
            switch (val) {
            case XXX:
                break;
            case YYY:
                break;
            default:
                break;
            }
        } else {
            y = y * x + 1;
        }
        
        return func2 (x, y);
    }

  * For consistency we expand tabs into spaces. This means no tabs in files
    unless there's a specific reason for it.
  * Break long lines where possible.
  * Spaces between functions and arguments, or macros and arguments.
  * Spaces before and after most binary operators. 
  * Spaces after most unary operators (including a comma).
  * Brace on same line as block statement. 
  * Single lined block statements don't need braces unless it makes things 
    significantly clearer. 
  * Return value on line before function definition.
  * Brace on line after function definition.
  * '*' goes with variable not type, when declaring a pointer.
  * Function arguments follow the function call on the same line, and if 
    lengthy are (where possible) wrapped to the column of the first brace.
 
last updated 2006-09-09
