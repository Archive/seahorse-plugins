/*
 * Seahorse
 *
 * Copyright (C) 2004 Stefan Walter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdlib.h>
#include <locale.h>
  
#include <glib/gi18n.h>

#include "seahorse-prefs.h"
#include "seahorse-gtkstock.h"

static const GOptionEntry options[] = {
	{ NULL }
};

G_MODULE_EXPORT static void
destroyed (gpointer widget, gpointer data)
{
	exit (0);
}

G_MODULE_EXPORT static void
help_clicked (GtkWidget *widget, SeahorseWidget *swidget)
{
	seahorse_widget_show_help (swidget);
}

int
main (int argc, char **argv)
{
    SeahorseWidget *swidget;
    GOptionContext *octx = NULL;
    GError *error = NULL;

    bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    textdomain (GETTEXT_PACKAGE);

    octx = g_option_context_new ("");
    g_option_context_add_main_entries (octx, options, GETTEXT_PACKAGE);

    if (!gtk_init_with_args (&argc, &argv, _("Encryption Preferences"), (GOptionEntry *) options, GETTEXT_PACKAGE, &error)) {
        g_printerr ("seahorse-preferences: %s\n", error->message);
        g_error_free (error);
        exit (1);
    }

    /* Insert Icons into Stock */ 
    seahorse_gtkstock_init();
    
    swidget = seahorse_prefs_new (NULL);
	g_signal_connect (seahorse_widget_get_toplevel (swidget), "destroy", 
                      G_CALLBACK (destroyed), NULL);
    g_signal_connect (seahorse_widget_get_widget (swidget, "closebutton1"), "clicked",
                      G_CALLBACK (destroyed), NULL);
    g_signal_connect (seahorse_widget_get_widget (swidget, "helpbutton1"), "clicked", G_CALLBACK (help_clicked), swidget);

	gtk_main();
	return 0;
}
