/*
 *  Copyright (C) 2000 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *  $Id$
 */

#ifndef SMART_BOOKMARKS_MOZILLA_SELECTION_H
#define SMART_BOOKMARKS_MOZILLA_SELECTION_H

#include <glib.h>

#include <epiphany/ephy-embed.h>

G_BEGIN_DECLS

gboolean	    mozilla_is_input            (EphyEmbed *embed);
const char*     mozilla_get_text            (EphyEmbed *embed);
void            mozilla_set_text            (EphyEmbed *embed,
                                             char *new_text);
G_END_DECLS

#endif
