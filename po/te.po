# translation of seahorse-plugins.master.te.po to Telugu
# Telugu translation of seahorse.
# Copyright (C) 2007,2011 Swecha Telugu Localisation Team <localization@swecha.net>.
# This file is distributed under the same license as the seahorse package.
#
# Srinivasa Chary <srinu.kolpur@gmail.com>, 2007.
# Krishna Babu K <kkrothap@redhat.com>, 2009.
# Hari Krishna <hari@swecha.net>, 2011.
# Krishna Babu K <kkrothap@redhat.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: seahorse-plugins.master.te\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=seahorse-plugins&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2011-08-31 14:49+0000\n"
"PO-Revision-Date: 2009-09-11 18:21+0530\n"
"Last-Translator: Hari Krishna <hari@swecha.net>\n"
"Language-Team: Telugu <indlinux-telugu@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"X-Generator: KBabel 1.11.4\n"

#: ../data/seahorse-plugins.schemas.in.h:1
msgid ""
"A list of key server URIs to search for remote PGP keys. In later versions a "
"display name can be included, by appending a space and then the name."
msgstr ""
"ఎడమైన పీజీపీ మీటల కోసం వెతకడానికి ముఖ్య సేవికల యూ ఆర్ ఐల జాబితా.తరువాతి వివరణాలలో ఒక ఖాళీ అక్షరాన్ని ఇచ్చి "
"పేరును ప్రద"

#: ../data/seahorse-plugins.schemas.in.h:2
msgid ""
"After performing a verify operation from the applet, display the resulting "
"text in a window."
msgstr ""
"ఆప్లెట్‌నుండి నిర్ధారణ ఆపరేషన్ జరిపిన తర్వాత, ఫలిత పాఠ్యమును విండోనందు ప్రదర్శించుము."

#: ../data/seahorse-plugins.schemas.in.h:3
msgid ""
"After performing an decrypt operation from the applet, display the resulting "
"text in a window."
msgstr ""
"ఆప్లెట్‌నుండి డిక్రిప్టు ఆపరేషన్ జరిపిన తర్వాత, ఫలిత పాఠ్యమును విండోనందు ప్రదర్శించుము."

#: ../data/seahorse-plugins.schemas.in.h:4
msgid ""
"After performing an encrypt or signing operation from the applet, display "
"the resulting text in a window."
msgstr ""
"యాప్ లెట్ నుం రహస్యపరచడం లేదా సంతకం చేసిన తరువాత , వచ్చిన ఫలితాన్ని మరో గవాక్షంలో ప్రదర్శించు."

#: ../data/seahorse-plugins.schemas.in.h:5
msgid "Display cache reminder in the notification area"
msgstr "ప్రకటన ప్రాంతమునందు క్యాచీ గుర్తుచేయుదానిని ప్రదర్శించుము"

#: ../data/seahorse-plugins.schemas.in.h:6
msgid "Display clipboard after decrypting"
msgstr "డిక్రిప్ట్ తర్వాత క్లిప్‌బోర్డు ప్రదర్శించుము"

#: ../data/seahorse-plugins.schemas.in.h:7
msgid "Display clipboard after encrypting"
msgstr "రహస్యపరచడం అయిన తరువాత క్లిప్ బోర్డును ప్రదర్శించు."

#: ../data/seahorse-plugins.schemas.in.h:8
msgid "Display clipboard after verifying"
msgstr "నిర్ధారణ తర్వాత క్లిప్‌బోర్డు ప్రదర్శించుము"

#: ../data/seahorse-plugins.schemas.in.h:9
msgid "Expire passwords in the cache"
msgstr "తాత్కాలిక స్థలంలోని సంకేతపదాలను ముగించు"

#: ../data/seahorse-plugins.schemas.in.h:10
msgid "ID of the default key"
msgstr "అప్రమేయ మీట యొక్క గుచి"

#: ../data/seahorse-plugins.schemas.in.h:11
msgid ""
"If set to 'gnome' uses gnome-keyring to cache passwords. When set to "
"'internal' uses internal cache."
msgstr ""
"గ్నోమ్ కు పెడితే సంకేతపదాలను తాత్కాలిక స్థలంలో‌ పెట్టడానికి గ్నోమ్-కీలకవలయాన్ని వాడుతుంది.'ఇంటర్నల్'కు పెడితే "
"అంతర్గత తాత్కాలిక స్థలాన్ని వాడుతుంది."

#: ../data/seahorse-plugins.schemas.in.h:12
msgid ""
"If set to true, then files encrypted with seahorse will be ASCII armor "
"encoded."
msgstr "సత్యమునకు అమర్చితే, సీహార్స్ తో ఎన్క్రిప్టైన ఫైళ్ళు ASCII ఆర్మర్ ఎన్కోడ్ చేయబడతాయి "

#: ../data/seahorse-plugins.schemas.in.h:13
msgid ""
"If set to true, then the default key will always be added to an encryption "
"recipients list."
msgstr ""
"సత్యమునకు అమర్చితే, అప్పుడు అప్రమేయ కీ యెల్లప్పుడూ ఎన్క్రిప్షన్ స్వీకరణకర్తల జాబితాకు జతచేయబడుతుంది."

#: ../data/seahorse-plugins.schemas.in.h:14
msgid "Last key used to sign a message."
msgstr "సంతకము చేయుటకు చివరిగా వాడిన మీట"

#: ../data/seahorse-plugins.schemas.in.h:15
msgid "PGP Key servers"
msgstr "పీజీపీ మీట సేవికలు"

#: ../data/seahorse-plugins.schemas.in.h:16
msgid "Prompt before using GPG passwords in cache"
msgstr "తాత్కాలిక స్థలంలో జీపీజీ సంకేతపదాలను వాడేముందు తెలియపరచు"

#: ../data/seahorse-plugins.schemas.in.h:17
msgid ""
"Reflect the contents of the clipboard (whether encrypted, signed, etc...) in "
"the panel applet icon."
msgstr ""
"క్లిప్ బోర్డులోనిది ప్యానల్ యాప్ లెట్ ప్రతిమలో ప్రతిఫలింపచేయు(రహస్యపరచి ఉన్న,సంతకం చేయబడి ఉన్న,మొదలైనవి...)."

#: ../data/seahorse-plugins.schemas.in.h:18
msgid ""
"Set to 'true' to enable display of the cache reminder in the notification "
"area of your panel."
msgstr ""
"మీ పానల్‌యొక్క ప్రకటన ప్రాంతమునందు క్యాచీ గుర్తుచేయుదానియొక్క ప్రదర్శనను చేతనంచేయుటకు 'సత్యము'కు "
"అమర్చుము."

#: ../data/seahorse-plugins.schemas.in.h:19
msgid ""
"Set to 'true' to have seahorse-agent prompt before giving out passwords it "
"has cached."
msgstr "తాత్కాలిక స్థలంలోని సంకేతపదాలను ఇచ్చేముందు సీహార్స్-కర్త మీకు చెప్పాలంటే దీనిని నిజానికి పెట్టండి"

#: ../data/seahorse-plugins.schemas.in.h:20
msgid "Show clipboard state in panel"
msgstr "ప్యానల్లో క్లిప్ బోర్డు పరిస్థితిని చూపు "

#: ../data/seahorse-plugins.schemas.in.h:21
msgid ""
"Specify the column to sort the recipients window by. Columns are: 'name' and "
"'id'. Put a '-' in front of the column name to sort in descending order."
msgstr ""
"పుచ్చుకునేవారి గవాక్షాన్నిచక్కదిద్దేందుకు నిలువును వివరముగా తెలియచేయండి.ఆ నిలువులు :'పేరు'మరియు "
"'గుచి'  .అవరోహణాక్రమంగా చక్కదిద్దేందుకు నిలువు ముందు ఒక '-'ను పెట్టండి."

#: ../data/seahorse-plugins.schemas.in.h:22
msgid "The ID of the last secret key used to sign a message."
msgstr "చివరిగా సందేశాన్ని సంతకం చేయడానికి వాడిన రహస్య మీట యొక్క గుచి."

#: ../data/seahorse-plugins.schemas.in.h:23
msgid "The column to sort the recipients by"
msgstr "పుచ్చుకునే వారిని చక్కదిద్దేందుకు నిలువు"

#: ../data/seahorse-plugins.schemas.in.h:24
msgid "The time (in minutes) to cache GPG passwords"
msgstr "జీపీజీ సంకేతపదాలను తాత్కాలిక స్థలంలో ఉంచడానికి సమయం(నిమిషాలలో)"

#: ../data/seahorse-plugins.schemas.in.h:25
msgid ""
"This is the amount of time, specified in minutes, to cache GPG passwords in "
"seahorse-agent."
msgstr ""
"ఇది సీహార్స్-కర్తలో  జీపీజీ సంకేతపదాలను తాత్కాలిక స్థలంలో ఉంచడానికి పట్టే సమయం నిమిషాలలో వివరంగా తెలియజేయబడింది"

#: ../data/seahorse-plugins.schemas.in.h:26
msgid ""
"This option enables the GPG password cache in the seahorse-agent program. "
"The 'use-agent' setting in gpg.conf affects this setting."
msgstr ""
"ఈ ఐచ్చికము సీహార్సు-ప్రతినిధి ప్రోగ్రామునందలి GPG సంకేతపద క్యాచీను చేతనపరుస్తుంది. gpg.conf అమరిక "
"నందలి 'use-agent' అమరిక ఈ అమరికను ప్రభావితం చేస్తుంది."

#: ../data/seahorse-plugins.schemas.in.h:27
msgid ""
"This specifies the default key to use for certain operations, mainly signing."
msgstr "ఇది కొన్ని కార్యాల కోసం, ముఖ్యంగా సంతకం చేయటానికి అప్రమేయ మీటను వివరిస్తుంది."

#: ../data/seahorse-plugins.schemas.in.h:28
msgid ""
"When set, seahorse-agent expires GPG passwords in its cache after a period "
"of time."
msgstr ""
"అమర్చినప్పుడు, కొంత సమయం తర్వాత సీహార్సు-ప్రతినిధి GPG సంకేతపదములను దాని క్యాచీనందు తీసివేయును."

#: ../data/seahorse-plugins.schemas.in.h:29
msgid "Where to store cached passwords."
msgstr "తాత్కాలిక స్థలంలో ఉన్న సంకేతపదాలను ఎక్కడ ఉంచాలి."

#: ../data/seahorse-plugins.schemas.in.h:30
msgid "Whether the GPG password cache is enabled"
msgstr "GPG సంకేతపదపు క్యాచీ చేతనపరచవలెనా"

#: ../data/seahorse-plugins.schemas.in.h:31
msgid "Whether to always encrypt to default key"
msgstr "ఎల్లప్పుడూ అప్రమేయకీనకు ఎన్క్రిప్టు చేయబడాలా"

#: ../data/seahorse-plugins.schemas.in.h:32
msgid "Whether to use ASCII Armor"
msgstr "ASCII Armorను వాడాలా వద్దా"

#: ../libseahorse/seahorse-multi-encrypt.xml.h:1
msgid "<b>You have selected multiple files or folders</b>"
msgstr "<b>మీరు ఎక్కువ ఫైళ్ళను లేదా  సంచయాలను ఎన్నుకొన్నారు</b>"

#: ../libseahorse/seahorse-multi-encrypt.xml.h:2
msgid ""
"Because the files are located remotely, each file will be encrypted "
"separately."
msgstr "ఎందుకంటే ఫైళ్ళు సుదూరంగా ఉన్నయి, ప్రతి ఫైలు వేరువేరుగా రహస్యపరచబడతాయి."

#: ../libseahorse/seahorse-multi-encrypt.xml.h:3
msgid "Encrypt Multiple Files"
msgstr "చాలా ఫైళ్ళను రహస్యపరచు"

#: ../libseahorse/seahorse-multi-encrypt.xml.h:4
msgid "Encrypt each file separately"
msgstr "ప్రతి ఫైలును వేరువేరుగా రహస్యపరచండి"

#: ../libseahorse/seahorse-multi-encrypt.xml.h:5
msgid "Encrypt packed together in a package"
msgstr "ప్యాకైనవి కలిపి సంకలనమునందు ఎన్క్రిప్టుచేయుము"

#: ../libseahorse/seahorse-multi-encrypt.xml.h:6
msgid "Package Name:"
msgstr "కట్ట పేరు:"

#: ../libseahorse/seahorse-multi-encrypt.xml.h:7
msgid "Packaging:"
msgstr "కట్ట:"

#: ../libseahorse/seahorse-multi-encrypt.xml.h:8
msgid "encrypted-package"
msgstr "రహస్యపరచిన-కట్ట"

#: ../libseahorse/seahorse-notification.c:583
#: ../libseahorse/seahorse-notification.c:608
msgid "Key Imported"
msgid_plural "Keys Imported"
msgstr[0] "మీట దిగుమతి చేయబడింది"
msgstr[1] "మీటలను దిగుమతి చేయబడినవి"

#: ../libseahorse/seahorse-notification.c:587
#: ../libseahorse/seahorse-notification.c:607
#, c-format
msgid "Imported %i key"
msgid_plural "Imported %i keys"
msgstr[0] "%i మీట దిగుమతి చేయబడింది"
msgstr[1] "%i మీటలు దిగుమతి చేయబడ్డాయి"

#: ../libseahorse/seahorse-notification.c:589
#, c-format
msgid "Imported a key for"
msgid_plural "Imported keys for"
msgstr[0] "మీట దిగుమతి చేయబడింది"
msgstr[1] "మీటలు దిగుమతి చేయబడ్డాయి"

#. TRANSLATORS: <key id='xxx'> is a custom markup tag, do not translate.
#: ../libseahorse/seahorse-notification.c:626
#, c-format
msgid "Signed by <i><key id='%s'/> <b>expired</b></i> on %s."
msgstr "<i><key id='%s'/>చే సంతకం చేయబడినది %s <b>ముగిసినది</b></i>"

#: ../libseahorse/seahorse-notification.c:627
msgid "Invalid Signature"
msgstr "నిస్సారమైన సంతకం"

#: ../libseahorse/seahorse-notification.c:633
#, c-format
msgid "Signed by <i><key id='%s'/></i> on %s <b>Expired</b>."
msgstr "<i><key id='%s'/>చే సంతకం చేయబడినది %s <b>ముగిసినది</b></i>"

#: ../libseahorse/seahorse-notification.c:634
msgid "Expired Signature"
msgstr "ముగిసిన సంతకం"

#: ../libseahorse/seahorse-notification.c:640
#, c-format
msgid "Signed by <i><key id='%s'/> <b>Revoked</b></i> on %s."
msgstr "<i><key id='%s'/>చే సంతకం చేయబడినది %s <b>కొట్టివేయబడింది</b></i>"

#: ../libseahorse/seahorse-notification.c:641
msgid "Revoked Signature"
msgstr "కొట్టివేయబడిన సంతకం"

#. TRANSLATORS: <key id='xxx'> is a custom markup tag, do not translate.
#: ../libseahorse/seahorse-notification.c:647
#, c-format
msgid "Signed by <i><key id='%s'/></i> on %s."
msgstr "%s పై <i><key id='%s'/></i>సంతకం చేయబడింది. "

#: ../libseahorse/seahorse-notification.c:648
msgid "Good Signature"
msgstr "మంచి సంతకం"

#: ../libseahorse/seahorse-notification.c:653
msgid "Signing key not in keyring."
msgstr "సంతకం చేయబడుతున్న మీట కీలకవలయంలో లేదు."

#: ../libseahorse/seahorse-notification.c:654
msgid "Unknown Signature"
msgstr "తెలియని సంతకం"

#: ../libseahorse/seahorse-notification.c:658
msgid "Bad or forged signature. The signed data was modified."
msgstr "చెడ్డ లేదా దొంగ సంతకము. సంతకంచేసిన డాటా సవరించబడింది."

#: ../libseahorse/seahorse-notification.c:659
msgid "Bad Signature"
msgstr "చెడ్డ సంతకం"

#: ../libseahorse/seahorse-notification.c:667
msgid "Couldn't verify signature."
msgstr "సంతకాన్ని పరీక్షించలేకపోయింది."

#: ../libseahorse/seahorse-notify.xml.h:1
msgid "Notification Messages"
msgstr "తాఖీదు సందేశాలు"

#: ../libseahorse/seahorse-passphrase.c:202
msgid "Passphrase"
msgstr "పాస్ పదసముదాయం"

#: ../libseahorse/seahorse-passphrase.c:205
msgid "Password:"
msgstr "సంకేతపదం:"

#: ../libseahorse/seahorse-passphrase.c:271
msgid "Confirm:"
msgstr "ధృఢపరచు:"

#: ../libseahorse/seahorse-passphrase.c:359
#, c-format
msgid "Wrong passphrase."
msgstr "తప్పు పాస్ పదసముదాయం."

#: ../libseahorse/seahorse-passphrase.c:363
#, c-format
msgid "Enter new passphrase for '%s'"
msgstr "'%s' కోసం కొత్త పాస్ పదసముదాయాన్ని ఇవ్వండి"

#: ../libseahorse/seahorse-passphrase.c:365
#, c-format
msgid "Enter passphrase for '%s'"
msgstr "'%s' కోసం పాస్ పదసముదాయాన్ని ఇవ్వండి"

#: ../libseahorse/seahorse-passphrase.c:368
msgid "Enter new passphrase"
msgstr "కొత్త పాస్ పదసముదాయాన్ని ఇవ్వండి"

#: ../libseahorse/seahorse-passphrase.c:370
msgid "Enter passphrase"
msgstr "పాస్ పదసముదాయాన్ని ఇవ్వండి"

#: ../libseahorse/seahorse-prefs.c:98
msgid "None. Prompt for a key."
msgstr "ఒకటీ లేదు.మీట కోసం తెలియపరచు."

#: ../libseahorse/seahorse-prefs.xml.h:1
msgid "<b>Default Key</b>"
msgstr "<b>అప్రమేయ కీ</b>"

#: ../libseahorse/seahorse-prefs.xml.h:2
msgid "<i>This key is used to sign messages when no other key is chosen</i>"
msgstr "<i>ఈ మీట వేరే </i>"

#: ../libseahorse/seahorse-prefs.xml.h:3
#: ../plugins/nautilus/seahorse-pgp-preferences.desktop.in.h:2
msgid "Encryption and Keyrings"
msgstr "ఎన్క్రిప్షన్ మరియు కీరింగ్‌లు"

#: ../libseahorse/seahorse-prefs.xml.h:4
msgid "PGP Passphrases"
msgstr "PGP సంకేతపదములు"

#: ../libseahorse/seahorse-prefs.xml.h:5
msgid "When _encrypting, always include myself as a recipient"
msgstr "ఎన్క్రిప్టింగ్ చేయునప్పుడు, ఎల్లప్పుడూ నన్నుకూడా స్వీకరించు వానిగా చేర్చుము"

#: ../libseahorse/seahorse-prefs.xml.h:6
msgid "_Default key:"
msgstr "అప్రమేయ కీ (_D):"

#: ../libseahorse/seahorse-progress.xml.h:1
msgid "Progress Title"
msgstr "పురోగమన శీర్షిక"

#: ../libseahorse/seahorse-util.c:173
msgid "Decryption failed. You probably do not have the decryption key."
msgstr "డిక్రిప్షన్ విఫలమైంది. మీరు బహుశా డిక్రిప్షన్ కీను కలిగిలేరు."

#: ../libseahorse/seahorse-util.c:223
msgid "%Y-%m-%d"
msgstr "%Y-%m-%d"

#: ../libseahorse/seahorse-util.c:642
msgid "Couldn't run file-roller"
msgstr "ఫైల్-రోలర్‌ను నడుపలేక పోయింది"

#: ../libseahorse/seahorse-util.c:647
#: ../plugins/nautilus/seahorse-tool-files.c:887
msgid "Couldn't package files"
msgstr "ఫైళ్ళను సంకలనము చేయలేకపోయింది"

#: ../libseahorse/seahorse-util.c:648
msgid "The file-roller process did not complete successfully"
msgstr "ఫైల్-రోలర్ కార్యక్రమము సమర్ధవంతంగా పూర్తికాలేదు"

#. Filter for PGP keys. We also include *.asc, as in many
#. cases that extension is associated with text/plain
#: ../libseahorse/seahorse-util.c:708
msgid "All key files"
msgstr "అన్ని ముఖ్యమైన ఫైళ్ళు"

#: ../libseahorse/seahorse-util.c:715 ../libseahorse/seahorse-util.c:755
msgid "All files"
msgstr "అన్ని ఫైళ్ళు"

#: ../libseahorse/seahorse-util.c:748
msgid "Archive files"
msgstr "సంగ్రహమైన ఫైళ్ళు"

#: ../libseahorse/seahorse-util.c:777
msgid ""
"<b>A file already exists with this name.</b>\n"
"\n"
"Do you want to replace it with a new file?"
msgstr ""
"<b>ఈ నామముతో వొక ఫైలు యిప్పటికే వుంది.</b>\n"
"\n"
"మీరు దీనిని కొత్త ఫైలుతో పునఃస్థాపించాలని అనుకొనుచున్నారా?"

#: ../libseahorse/seahorse-util.c:780
msgid "_Replace"
msgstr "పునఃస్థాపించు (_R)"

#: ../libseahorse/seahorse-widget.c:366
#, c-format
msgid "Could not display help: %s"
msgstr "%s సహాయాన్ని ప్రదర్శించలేకపోయింది"

#: ../plugins/applet/seahorse-applet.c:284
msgid "seahorse-applet"
msgstr "సీహార్స్-యాప్ లెట్"

#: ../plugins/applet/seahorse-applet.c:286
#: ../plugins/applet/seahorse-applet.c:867
msgid "Use PGP/GPG to encrypt/decrypt/sign/verify/import the clipboard."
msgstr "క్లిప్‌బోర్డు encrypt/decrypt/sign/verify/import నకు PGP/GPG వుపయోగించుము."

#: ../plugins/applet/seahorse-applet.c:291
msgid "translator-credits"
msgstr "Srinivasa Chary <srinu.kolpur@gmail.com>"

#: ../plugins/applet/seahorse-applet.c:294
msgid "Seahorse Project Homepage"
msgstr "సీహార్స్ పథకం నివాస పుట"

#. Get the recipient list
#: ../plugins/applet/seahorse-applet.c:366
#: ../plugins/epiphany/seahorse-extension.c:272
#: ../plugins/gedit/seahorse-gedit.c:338
msgid "Choose Recipient Keys"
msgstr "పుచ్చుకునే మీటలను ఎన్నుకోండి"

#: ../plugins/applet/seahorse-applet.c:385
msgid "Encrypted Text"
msgstr "రహస్యపరచిన పాఠం"

#: ../plugins/applet/seahorse-applet.c:387
msgid "Encryption Failed"
msgstr "రహస్యపరచడం నిఫలమైంది"

#: ../plugins/applet/seahorse-applet.c:387
msgid "The clipboard could not be encrypted."
msgstr "క్లిప్ బోర్డు రహస్యపరచడలేదు."

#: ../plugins/applet/seahorse-applet.c:425
#: ../plugins/epiphany/seahorse-extension.c:322
#: ../plugins/gedit/seahorse-gedit.c:628
msgid "Choose Key to Sign with"
msgstr "సంతకం చేయటానికి మీటను ఎన్నుకోండి"

#: ../plugins/applet/seahorse-applet.c:445
msgid "Signed Text"
msgstr "సంతకం చేయబడ్డ పాఠం"

#: ../plugins/applet/seahorse-applet.c:447
#: ../plugins/applet/seahorse-applet.c:527
msgid "Signing Failed"
msgstr "సంతకం చేయడం విఫలమైంది"

#: ../plugins/applet/seahorse-applet.c:447
#: ../plugins/applet/seahorse-applet.c:527
msgid "The clipboard could not be Signed."
msgstr "క్లిప్ బోర్డును సంతకం చేయబడలేదు."

#: ../plugins/applet/seahorse-applet.c:489
#: ../plugins/epiphany/seahorse-extension.c:366
#: ../plugins/nautilus/seahorse-tool.c:471
msgid "Import Failed"
msgstr "దిగుమతి విఫలమైంది"

#: ../plugins/applet/seahorse-applet.c:490
#: ../plugins/epiphany/seahorse-extension.c:367
#: ../plugins/nautilus/seahorse-tool.c:472
msgid "Keys were found but not imported."
msgstr "మీటలు దొరికినవి కానీ దిగుమతి చేయబడలేదు."

#: ../plugins/applet/seahorse-applet.c:582
msgid "No PGP key or message was found on clipboard"
msgstr "క్లిప్ బోర్డులో పీజీపీ మీట లేదా సందేశం ఏదీ దొరకలేదు"

#: ../plugins/applet/seahorse-applet.c:583
msgid "No PGP data found."
msgstr "పీజీపీ దత్తాంశం ఏదీ దొరకలేదు."

#. TRANSLATORS: This means 'The text that was decrypted'
#: ../plugins/applet/seahorse-applet.c:620
msgid "Decrypted Text"
msgstr "పాఠ్యమును డీక్రిప్టుచేయుము"

#: ../plugins/applet/seahorse-applet.c:712
#, c-format
msgid "Could not display URL: %s"
msgstr "URL ప్రదర్శించలేక పోయింది: %s"

#: ../plugins/applet/seahorse-applet.c:782
msgid "_Encrypt Clipboard"
msgstr "రహస్యపరచు క్లిప్ బోర్డు (_E)"

#: ../plugins/applet/seahorse-applet.c:789
msgid "_Sign Clipboard"
msgstr "క్లిప్ బోర్డును సంతకం చేయు (_S)"

#: ../plugins/applet/seahorse-applet.c:796
msgid "_Decrypt/Verify Clipboard"
msgstr "క్లిప్‌బోర్డు డీక్రిప్టుచేయి/నిర్ధారించుము (_D)"

#: ../plugins/applet/seahorse-applet.c:802
msgid "_Import Keys from Clipboard"
msgstr "క్లిప్ బోర్డు నుంచి మీటలను దిగుమతిచేయు (_I)"

#: ../plugins/applet/seahorse-applet.c:864
#: ../plugins/applet/seahorse-applet.c:866
msgid "Encryption Applet"
msgstr "రహస్యపరచు యాప్ లెట్"

#: ../plugins/applet/seahorse-applet.c:934
msgid "_Preferences"
msgstr "అభీష్టాలు (_P)"

#: ../plugins/applet/seahorse-applet.c:937
msgid "_Help"
msgstr "సహాయం (_H)"

#: ../plugins/applet/seahorse-applet.c:940
msgid "_About"
msgstr "గురించి (_A)"

#.
#. PANEL_APPLET_OUT_PROCESS_FACTORY ("SeahorseAppletFactory",
#. SEAHORSE_TYPE_APPLET, "SeahorseApplet",
#. seahorse_applet_factory, NULL);
#.
#: ../plugins/applet/seahorse-applet-preferences.xml.h:1
msgid "<b>Display clipboard contents after:</b>"
msgstr "<b>క్ల్లిప్ బోర్డు యొక్క వివరములను ఎంత సమయం తరువాత ప్రదర్శించాలి:</b>"

#: ../plugins/applet/seahorse-applet-preferences.xml.h:2
msgid "Clipboard Encryption Preferences"
msgstr "క్లిప్ బోర్డు  రహస్యపరచు అభీష్టాలు"

#: ../plugins/applet/seahorse-applet-preferences.xml.h:3
msgid "_Decrypting the clipboard"
msgstr "క్లిప్‌బోర్డు డీక్రిప్టుచేయుచున్నది (_D)"

#: ../plugins/applet/seahorse-applet-preferences.xml.h:4
msgid "_Encrypting or signing the clipboard"
msgstr "క్లిప్ బోర్డును రహస్యపరచు లేదా సంతకం చేయు (_E)"

#: ../plugins/applet/seahorse-applet-preferences.xml.h:5
msgid "_Show clipboard state in panel"
msgstr "క్లిప్ బోర్డు స్థితిని ప్యానలల్లో చూపు (_S)"

#: ../plugins/applet/seahorse-applet-preferences.xml.h:6
msgid "_Verifying the clipboard"
msgstr "క్లిప్‌బోర్డును నిర్ధారించుచున్నది (_V)"

#: ../plugins/epiphany/seahorse-extension.c:405
msgid "Decrypting Failed"
msgstr "డీక్రిప్టుచేయుట విఫలమైంది"

#: ../plugins/epiphany/seahorse-extension.c:405
msgid "Text may be malformed."
msgstr "పాఠ్యము చెడ్డగా వుండివుండవచ్చు."

#: ../plugins/epiphany/seahorse-extension.c:629
msgid "_Encrypt"
msgstr "రహస్యపరచు (_E)"

#: ../plugins/epiphany/seahorse-extension.c:636
msgid "_Sign"
msgstr "సంతకం (_S)"

#: ../plugins/epiphany/seahorse-extension.c:643
msgid "_Decrypt/Verify"
msgstr "డిక్రిప్టు/నిర్ధారణ (_D)"

#: ../plugins/epiphany/seahorse-extension.c:650
msgid "_Import Key"
msgstr "మీటను దిగుమతి చేయు (_I)"

#: ../plugins/gedit/seahorse-gedit.c:268
msgid "Couldn't connect to seahorse-daemon"
msgstr "సీహార్స్-సూత్రధారికి బంధించలేకపోయింది"

#: ../plugins/gedit/seahorse-gedit.c:360
msgid "Encrypted text"
msgstr "రహస్యపరచబడ్డ పాఠం"

#: ../plugins/gedit/seahorse-gedit.c:363
msgid "Couldn't encrypt text"
msgstr "పాఠాన్ని రహస్యపరచలేకపోయింది"

#: ../plugins/gedit/seahorse-gedit.c:393
msgid "Couldn't import keys"
msgstr "మీటలను దిగుమతి చేయలేకపోయింది"

#: ../plugins/gedit/seahorse-gedit.c:402
msgid "Keys found but not imported"
msgstr "మీటలు దొరికినవి కానీ దిగుమతి చేయబడలేదు"

#: ../plugins/gedit/seahorse-gedit.c:439
msgid "Couldn't decrypt text"
msgstr "పాఠ్యమును డీక్రిప్టు చేయలేక పోయింది"

#: ../plugins/gedit/seahorse-gedit.c:476
msgid "Couldn't verify text"
msgstr "పాఠాన్ని తనిఖీ చేయలేకపోయింది"

#: ../plugins/gedit/seahorse-gedit.c:524
msgid "No encrypted or signed text is selected."
msgstr "రహస్యపరచబడ్డ లేదా సంతకం చేయబడ్డ ఏదీ ఎన్నుకొనబడలేదు."

#: ../plugins/gedit/seahorse-gedit.c:554
msgid "Decrypted text"
msgstr "డీక్రిప్టుచేసిన పాఠ్యము"

#: ../plugins/gedit/seahorse-gedit.c:561
msgid "Verified text"
msgstr "తనిఖీ చేయబడ్డ పాఠం"

#: ../plugins/gedit/seahorse-gedit.c:596
#: ../plugins/nautilus/seahorse-tool.c:497
#, c-format
msgid "Imported %d key"
msgid_plural "Imported %d keys"
msgstr[0] "%d మీట దిగుమతి చేయబడింది"
msgstr[1] "%d మీటలు దిగుమతి చేయబడ్డాయి"

#: ../plugins/gedit/seahorse-gedit.c:650
msgid "Signed text"
msgstr "సంతకం చేయబడ్డ పాఠం"

#: ../plugins/gedit/seahorse-gedit.c:653
msgid "Couldn't sign text"
msgstr "పాఠాన్ని సంతకం చేయలేకపోయింది"

#: ../plugins/gedit/seahorse-gedit-plugin.c:133
msgid "_Encrypt..."
msgstr "రహస్యపరచు... (_E)"

#: ../plugins/gedit/seahorse-gedit-plugin.c:134
msgid "Encrypt the selected text"
msgstr "ఎన్నుకొన్న పాఠాన్ని రహస్యపరచు"

#: ../plugins/gedit/seahorse-gedit-plugin.c:135
msgid "Decr_ypt/Verify"
msgstr "డీక్రిప్టుచేయి/నిర్ధారించుము (_y)"

#: ../plugins/gedit/seahorse-gedit-plugin.c:136
msgid "Decrypt and/or Verify text"
msgstr "పాఠ్యము డీక్రిప్టుచేయి మరియు/లేదా నిర్ధారించుము"

#: ../plugins/gedit/seahorse-gedit-plugin.c:137
msgid "Sig_n..."
msgstr "సంతకం... (_n)"

#: ../plugins/gedit/seahorse-gedit-plugin.c:138
msgid "Sign the selected text"
msgstr "ఎంపికచేసిన పాఠ్యమును సంతకంచేయుము"

#: ../plugins/gedit/seahorse-gedit.schemas.in.h:1
msgid "Enable the seahorse encryption plugin for gedit."
msgstr "సరికూర్చుట కొరకు సీహార్సు ఎన్క్రిప్షన్ ప్లగిన్‌ చేతనము చేయుము."

#: ../plugins/gedit/seahorse-gedit.schemas.in.h:2
msgid "Enable the seahorse gedit plugin"
msgstr "సీహార్సు gedit ప్లగిన్ చేతనముచేయి"

#: ../plugins/gedit/seahorse-pgp.gedit-plugin.desktop.in.h:1
msgid "Text Encryption"
msgstr "పాఠ్యము ఎన్క్రిప్షన్"

#: ../plugins/gedit/seahorse-pgp.gedit-plugin.desktop.in.h:2
msgid "This plugin performs encryption operations on text."
msgstr "ఈ ప్లగిన్ యెన్క్రిప్షన్ ఆపరేషన్లను పాఠ్యముపై జరుపుతుంది."

#: ../plugins/nautilus-ext/seahorse-nautilus.c:158
msgid "Encrypt..."
msgstr "రహస్యపరచు..."

#: ../plugins/nautilus-ext/seahorse-nautilus.c:159
msgid "Encrypt (and optionally sign) the selected file"
msgid_plural "Encrypt the selected files"
msgstr[0] "ఎంపికచేసిన ఫైలు ఎన్క్రిప్టుచేయి (మరియు ఐచ్చికంగా సంతకంచేయి)"
msgstr[1] "ఎంపికచేసిన ఫైళ్ళను ఎన్క్రిప్టుచేయి"

#: ../plugins/nautilus-ext/seahorse-nautilus.c:165
msgid "Sign"
msgstr "సంతకం"

#: ../plugins/nautilus-ext/seahorse-nautilus.c:166
msgid "Sign the selected file"
msgid_plural "Sign the selected files"
msgstr[0] "ఎన్నుకొన్న ఫైలును సంతకం చేయు"
msgstr[1] "ఎన్నుకొన్న ఫైళ్ళను సంతకం చేయు"

#: ../plugins/nautilus/seahorse-pgp-preferences.c:62
msgid "Encryption Preferences"
msgstr "రహస్యపరచు అభీష్టాలు"

#: ../plugins/nautilus/seahorse-pgp-preferences.desktop.in.h:1
msgid "Configure key servers and other encryption settings"
msgstr "కీ సేవికలను మరియు యితర ఎన్క్రిప్షన్ అమరికలను ఆకృతీకరించుము"

#: ../plugins/nautilus/seahorse-pgp-encrypted.desktop.in.in.h:1
msgid "Decrypt File"
msgstr "ఫైలును డీక్రిప్టుచేయి"

#: ../plugins/nautilus/seahorse-pgp-keys.desktop.in.in.h:1
msgid "Import Key"
msgstr "కీను దిగుమతిచేయి"

#: ../plugins/nautilus/seahorse-pgp-signature.desktop.in.in.h:1
msgid "Verify Signature"
msgstr "సంతకాన్ని ధృవీకరించు"

#: ../plugins/nautilus/seahorse-tool.c:62
msgid "Import keys from the file"
msgstr "మీటలను ఫైలు నుంచి దిగుమతిచేయు"

#: ../plugins/nautilus/seahorse-tool.c:64
msgid "Encrypt file"
msgstr "ఫైలును రహస్యపరచు"

#: ../plugins/nautilus/seahorse-tool.c:66
msgid "Sign file with default key"
msgstr "ఫైలును అప్రమేయ మీటతో సంతకం చేయు"

#: ../plugins/nautilus/seahorse-tool.c:68
msgid "Encrypt and sign file with default key"
msgstr "ఫైలును రహస్యపరచి అప్రమేయ మీటతో సంతకం చేయు"

#: ../plugins/nautilus/seahorse-tool.c:70
msgid "Decrypt encrypted file"
msgstr "ఎన్క్రిప్టుచేసిన ఫైలును డీక్రిప్టుచేయుము"

#: ../plugins/nautilus/seahorse-tool.c:72
msgid "Verify signature file"
msgstr "సంతక ఫైలును పరీక్షించు"

#: ../plugins/nautilus/seahorse-tool.c:74
msgid "Read list of URIs on standard in"
msgstr "యూ ఆర్ ఐ ల జాబితాను ప్రమాణంలో చదవండి"

#: ../plugins/nautilus/seahorse-tool.c:76
msgid "file..."
msgstr "ఫైలు..."

#: ../plugins/nautilus/seahorse-tool.c:170
msgid "Choose Recipients"
msgstr "పుచ్చుకునేవారిని ఎన్నుకోండి"

#: ../plugins/nautilus/seahorse-tool.c:235
#: ../plugins/nautilus/seahorse-tool.c:348
msgid "Couldn't load keys"
msgstr "కీలను లోడుచేయలేక పోయింది"

#: ../plugins/nautilus/seahorse-tool.c:253
#, c-format
msgid "Choose Encrypted File Name for '%s'"
msgstr "'%s' కొరకు రహస్యపరచిన ఫైలు పేరును ఎన్నుకోండి"

#: ../plugins/nautilus/seahorse-tool.c:326
msgid "Choose Signer"
msgstr "సంతకదారిని ఎన్నుకోండి"

#: ../plugins/nautilus/seahorse-tool.c:368
#, c-format
msgid "Choose Signature File Name for '%s'"
msgstr "'%s' కొరకు సంతక ఫైలు పేరును ఎన్నుకోండి"

#: ../plugins/nautilus/seahorse-tool.c:408
msgid "Import is complete"
msgstr "దిగుమతి పూర్తైనది"

#: ../plugins/nautilus/seahorse-tool.c:441
msgid "Importing keys ..."
msgstr "కీలను దిగుమతిచేయుచున్నది ..."

#: ../plugins/nautilus/seahorse-tool.c:495
#, c-format
msgid "Imported key"
msgstr "దిగుమతి చేయబడ్డ మీట"

#. File to decrypt to
#: ../plugins/nautilus/seahorse-tool.c:519
#, c-format
msgid "Choose Decrypted File Name for '%s'"
msgstr "'%s' కొరకు డిక్రిప్టుచేసిన ఫైలు పేరును యెంచుకొనుము"

#: ../plugins/nautilus/seahorse-tool.c:576
#, c-format
msgid "Choose Original File for '%s'"
msgstr "'%s' కొరకు అసలు ఫైలును ఎన్నుకోండి"

#.
#. * TODO: What should happen with multiple files at this point.
#. * The last thing we want to do is cascade a big pile of error
#. * dialogs at the user.
#.
#: ../plugins/nautilus/seahorse-tool.c:649
#, c-format
msgid "No valid signatures found"
msgstr "సారమైన సంతకాలు కనబడలేదు"

#: ../plugins/nautilus/seahorse-tool.c:698
msgid "File Encryption Tool"
msgstr "ఫైలును రహస్యపరచే పనిముట్టు"

#: ../plugins/nautilus/seahorse-tool.c:717
msgid "Encrypting"
msgstr "రహస్యపరచడం"

#: ../plugins/nautilus/seahorse-tool.c:718
#, c-format
msgid "Couldn't encrypt file: %s"
msgstr "%s ను రహస్యపరచలేకపోయింది"

#: ../plugins/nautilus/seahorse-tool.c:726
msgid "Signing"
msgstr "సంతకం చేస్తోంది"

#: ../plugins/nautilus/seahorse-tool.c:727
#, c-format
msgid "Couldn't sign file: %s"
msgstr "%s ఫైలును సంతకం చేయలేకపోయింది"

#: ../plugins/nautilus/seahorse-tool.c:732
msgid "Importing"
msgstr "దిగుమతి చేస్తోంది"

#: ../plugins/nautilus/seahorse-tool.c:733
#, c-format
msgid "Couldn't import keys from file: %s"
msgstr "%s ఫైలు నుంచి మీటలను దిగుమతి చేయలేకపోయింది"

#: ../plugins/nautilus/seahorse-tool.c:739
msgid "Decrypting"
msgstr "డిక్రిప్టుచేయుచున్నది"

#: ../plugins/nautilus/seahorse-tool.c:740
#, c-format
msgid "Couldn't decrypt file: %s"
msgstr "ఫైలును డిక్రిప్టు చేయలేకపోయింది: %s"

#: ../plugins/nautilus/seahorse-tool.c:745
msgid "Verifying"
msgstr "పరీక్షిస్తోంది"

#: ../plugins/nautilus/seahorse-tool.c:746
#, c-format
msgid "Couldn't verify file: %s"
msgstr "%s దస్తాన్ని పరీక్షించలేకపోయింది"

#: ../plugins/nautilus/seahorse-tool-files.c:120
msgid "Ace (.ace)"
msgstr "ఏస్ (.ace)"

#: ../plugins/nautilus/seahorse-tool-files.c:121
msgid "Ar (.ar)"
msgstr "ఆర్(.ar)"

#: ../plugins/nautilus/seahorse-tool-files.c:122
msgid "Arj (.arj)"
msgstr "ఆర్జ్(.arj)"

#: ../plugins/nautilus/seahorse-tool-files.c:129
msgid "Ear (.ear)"
msgstr "ఇఅర్(.ear)"

#: ../plugins/nautilus/seahorse-tool-files.c:130
msgid "Self-extracting zip (.exe)"
msgstr "స్వయంగా-బయల్పడు జిప్ (.exe)"

#: ../plugins/nautilus/seahorse-tool-files.c:132
msgid "Jar (.jar)"
msgstr "జార్(.jar)"

#: ../plugins/nautilus/seahorse-tool-files.c:133
msgid "Lha (.lzh)"
msgstr "ల్హ(.lzh)"

#: ../plugins/nautilus/seahorse-tool-files.c:135
msgid "Rar (.rar)"
msgstr "రార్(.rar)"

#: ../plugins/nautilus/seahorse-tool-files.c:137
msgid "Tar uncompressed (.tar)"
msgstr "టార్ సంకుచితము చెందలేదు(.టార్)"

#: ../plugins/nautilus/seahorse-tool-files.c:138
msgid "Tar compressed with bzip (.tar.bz)"
msgstr "బీజిప్ తో అణచబడిన టార్(.tar.bz)"

#: ../plugins/nautilus/seahorse-tool-files.c:139
msgid "Tar compressed with bzip2 (.tar.bz2)"
msgstr "బీజిప్2 తో అణచబడిన టార్(.tar.bz2)"

#: ../plugins/nautilus/seahorse-tool-files.c:140
msgid "Tar compressed with gzip (.tar.gz)"
msgstr "జీజిప్ తో అణచబడిన టార్(.tar.gzip)"

#: ../plugins/nautilus/seahorse-tool-files.c:141
msgid "Tar compressed with lzop (.tar.lzo)"
msgstr "ఎల్ జాప్ తో అణచబడిన టార్(.tar.lzo)"

#: ../plugins/nautilus/seahorse-tool-files.c:142
msgid "Tar compressed with compress (.tar.Z)"
msgstr "కంప్రెస్ తో అణచబడిన టార్(.tar.Z)"

#: ../plugins/nautilus/seahorse-tool-files.c:144
msgid "War (.war)"
msgstr "వార్(.war)"

#: ../plugins/nautilus/seahorse-tool-files.c:145
msgid "Zip (.zip)"
msgstr "జిప్(.zip)"

#: ../plugins/nautilus/seahorse-tool-files.c:146
msgid "Zoo (.zoo)"
msgstr "జూ (.zoo)"

#: ../plugins/nautilus/seahorse-tool-files.c:147
msgid "7-Zip (.7z)"
msgstr "7-జిప్(.7z)"

#. TRANSLATOR: This string will become
#. * "You have selected %d files and %d folders"
#: ../plugins/nautilus/seahorse-tool-files.c:327
#, c-format
msgid "You have selected %d file "
msgid_plural "You have selected %d files "
msgstr[0] "మీరు %d ఫైలును ఎన్నుకొన్నారు"
msgstr[1] "మీరు %d ఫైళ్ళను ఎన్నుకొన్నారు"

#. TRANSLATOR: This string will become
#. * "You have selected %d files and %d folders"
#: ../plugins/nautilus/seahorse-tool-files.c:332
#, c-format
msgid "and %d folder"
msgid_plural "and %d folders"
msgstr[0] "మరియు %d సంచయం"
msgstr[1] "మరియు %d సంచయాలు"

#. TRANSLATOR: "%s%s" are "You have selected %d files and %d folders"
#. * Swap order with "%2$s%1$s" if needed
#: ../plugins/nautilus/seahorse-tool-files.c:337
#, c-format
msgid "<b>%s%s</b>"
msgstr "<b>%s%s</b>"

#: ../plugins/nautilus/seahorse-tool-files.c:345
msgid "You have selected %d file"
msgid_plural "You have selected %d files"
msgstr[0] "మీరు %d ఫైలును ఎన్నుకొన్నారు"
msgstr[1] "మీరు %d ఫైళ్ళను ఎన్నుకొన్నారు"

#: ../plugins/nautilus/seahorse-tool-files.c:350
#, c-format
msgid "You have selected %d folder"
msgid_plural "You have selected %d folders"
msgstr[0] "మీరు %d సంచయాన్ని ఎన్నుకొన్నారు"
msgstr[1] "మీరు %d సంచయాలను ఎన్నుకొన్నారు"

#: ../plugins/nautilus/seahorse-tool-files.c:868
msgid "Preparing..."
msgstr "తయారవుతోంది..."

#: ../plugins/nautilus/seahorse-tool-files.c:877
#: ../plugins/nautilus/seahorse-tool-files.c:900
msgid "Couldn't list files"
msgstr "ఫైళ్ళను జాబితా చేయలేకపోయింది"

#~ msgid "Passphrase:"
#~ msgstr "సంకేతపదము:"

#~ msgid "Please enter a passphrase to use."
#~ msgstr "దయచేసి ఒక పాస్ పదసముదాయం వాడుటకు చేర్చుము."

#~ msgid "Unparseable Key ID"
#~ msgstr "వ్యాకరించలేని మీట గుచి"

#~ msgid "Unknown/Invalid Key"
#~ msgstr "అపరిచితము /నిస్సారమైన మీట"

#~ msgid "PGP Key: %s"
#~ msgstr "పీజీపీ మీట:%s"

#~ msgid ""
#~ "<b>Warning</b>: Your system is not configured to cache passphrases in "
#~ "secure memory."
#~ msgstr "<b>హెచ్చరిక</b>: రక్షత మెమొరీనందు క్యాచీ చేయుటకు మీ సిస్టమ్ ఆకృతీకరించబడలేదు."

#~ msgid "Cache _Preferences"
#~ msgstr "తాత్కాలిక స్థలం_అభీష్టాలు"

#~ msgid "Cached Encryption Keys"
#~ msgstr "తాత్కాలిక స్థలాన్ని రహస్యపరచే మీటలు"

#~ msgid "Change passphrase cache settings."
#~ msgstr "పాస్ పదసముదాయ తాత్కాలిక స్థల అమరికలను మార్చు"

#~ msgid "Clear passphrase cache"
#~ msgstr "పాస్ పదసముదాయ తాత్కాలిక స్థలాన్ని శుభ్రము చేయు"

#~ msgid "_Clear Cache"
#~ msgstr "_తాత్కాలిక స్థలాన్ని శుభ్రము చేయు"

#~ msgid "_Show Window"
#~ msgstr "_గవాక్షాన్ని చూపు"

#~ msgid "Do not daemonize seahorse-agent"
#~ msgstr "సీహార్స్-కర్తను సూత్రధారిగా చేయవద్దు"

#~ msgid "Print variables in for a C type shell"
#~ msgstr "చరరాశులను C రకమైన షెల్ కొరకు ముద్రించు"

#~ msgid "Display environment variables (the default)"
#~ msgstr "ఎన్విరాన్మెంట్ చరరాశులను ప్రదర్శించుము (అప్రమేయ)"

#~ msgid "Execute other arguments on the command line"
#~ msgstr "ఇతర క్రమానుగత సంకేతాలను ఆదేశవాక్యంలో నిర్వర్తించు"

#~ msgid "Allow GPG agent request from any display"
#~ msgstr "ఏదైనా ప్రదర్శనం నుంచి జీపీజీ కర్త విన్నపాన్ని అనుమతించండి"

#~ msgid "command..."
#~ msgstr "ఆదేశం..."

#~ msgid "couldn't fork process"
#~ msgstr "ప్రోసెస్‌ను ఫోర్కు చేయలేకపోయింది"

#~ msgid "couldn't create new process group"
#~ msgstr "కొత్త క్రమణసముదాయాన్ని సృష్టించలేకపోయింది"

#~ msgid "Encryption Key Agent (Seahorse)"
#~ msgstr "రహస్యపరచు మీట కర్త (సీహార్స్)"

#~ msgid "no command specified to execute"
#~ msgstr "నిర్వర్తించటానికి ఆదేశం ఇవ్వలేదు"

#~ msgid "Authorize Passphrase Access"
#~ msgstr "పాస్ పదసముదాయ సాంగత్యానికి అధికారమివ్వు"

#~ msgid "The passphrase is cached in memory."
#~ msgstr "పాస్ పదసముదాయాన్ని జ్ఞాపకశక్తిలో తాత్కాలికంగా పెట్టబడింది"

#~ msgid "Always ask me before using a cached passphrase"
#~ msgstr "తాత్కాలికంగా పెట్టబడిన పాస్ పదసముదాయాన్ని వాడే ముందు ఎల్లప్పుడు నన్ను అడుగు"

#~ msgid "_Authorize"
#~ msgstr "_అధికారమివ్వుట"

#~ msgid "Key Name"
#~ msgstr "మీట యొక్క పేరు"

#~ msgid "<b>Remember PGP Passphrases</b>"
#~ msgstr "<b>పీజీపీ పాస్ పదసముదాయాలను గుర్తుంచుకోండి</b>"

#~ msgid "<i>A supported PGP passphrase caching agent is not running.</i>"
#~ msgstr "<i>మద్దతిచ్చునటువంటి PGP సంకేతపదము క్యచింగ్ ప్రతినిధి నడుచుటలేదు.</i>"

#~| msgid "_Ask me before using a cached passphrase"
#~ msgid "As_k me before using a cached passphrase"
#~ msgstr "క్యాచీచేసిన సంకేతపదమును వుపయోగించుటకు ముందుగా నన్ను అడుగుము (_k)"

#~| msgid "Encrypting"
#~ msgid "Encryption"
#~ msgstr "ఎన్క్రిప్షన్"

#~ msgid "Show _icon in status area when passphrases are in memory"
#~ msgstr "సంకేతపదములు మెమొరీనందు వున్నప్పుడు స్థితి ప్రాంతమునందు ప్రతిమను చూపుము (_i)"

#~ msgid "_Always remember passphrases whenever logged in"
#~ msgstr "ఎప్పుడు లాగినైనా యెల్లప్పుడూ సంకేతపదములను గుర్తుంచుకొనుము (_A)"

#~ msgid "_Never remember passphrases"
#~ msgstr "సంకేతపదములను యెప్పుడూ గుర్తుంచుకొనవద్దు (_N)"

#~ msgid "_Remember passphrases for"
#~ msgstr "సంకేతపదములను దీనికొరకు గుర్తుంచుకొనుము (_R)"

#~ msgid "minutes"
#~ msgstr "నిమిషాలు"

#~ msgid "Clipboard Text Encryption"
#~ msgstr "క్లిప్ బోర్డు పాఠం రహస్యపరచడం"

#~ msgid "Encrypt, decrypt or sign the clipboard (uses PGP type encryption)."
#~ msgstr "క్లిప్‌బోర్డు ఎన్క్రిప్టు, డీక్రిప్టు లేదా సంతకం (PGP రకము ఎన్క్రిప్షన్ వుపయోగిస్తుంది)."

#~ msgid "Seahorse Applet Factory"
#~ msgstr "సీహార్స్-యాప్ లెట్ కర్మాగారము"

#~ msgid "For internal use"
#~ msgstr "అంతర్గత  వాడుక కోసం"

#~ msgid "You have selected %d files"
#~ msgstr "మీరు %d ఫైళ్ళను ఎన్నుకొన్నారు"



